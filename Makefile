IMAGE = illustrations-emotions-besoins

all: hugo

clean:
	rm -rf resources

build:
	docker build --pull -t ${IMAGE} .

test: docker docker-network
	docker run -it --network azae-net registry.gitlab.com/azae/docker/linkchecker linkchecker --anchors --ignore-url="^mailto:" http://azae/
	docker run -it --network azae-net registry.gitlab.com/azae/docker/pa11y pa11y-ci --sitemap http://azae/sitemap.xml --config /etc/pa11y.conf --sitemap-find xn--aza-dma.net --sitemap-replace azae --sitemap-exclude pdf

	docker stop azae
	-docker rm -f azae
	-docker network rm azae-net

docker: build docker-network
	-docker stop azae
	-docker rm -f azae
	@docker run --name azae -d -p 8080:80 -p 8443:443 --network azae-net ${IMAGE}

docker-network:
	-docker network create --driver bridge azae-net

hugo:
	hugo server --buildDrafts --buildFuture --watch --bind 0.0.0.0 --port 8000

docker-dev: build docker-network
	-docker rm -f azae
	docker run --name azae -p 8000:8000 -p 8080:80 -p 8443:443 --rm -it -v $(shell pwd):/site --entrypoint=/bin/bash --network azae-net ${IMAGE}

.PHONY: hugo
