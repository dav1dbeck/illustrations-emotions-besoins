---
title: "Comment participer ?"
---

## Les illustrations

Préférez des images carrées, leur intégration sur le site n'en sera que plus simple.

## Sur [twitter](https://twitter.com/hashtag/IllustrationCNVLibre) ou [fediverse](https://mamot.fr/tags/IllustrationCNVLibre)

Rien de plus simple, partager votre contribution avec le mot diese #IllustrationCNVLibre, vous pouvez ajouter [Thomas Clavier sur Twitter](https://twitter.com/thomasclavier/) ou [Thomas Clavier sur Mastodon](https://mastodon.libre-entreprise.com/@Thomas) en copie pour une réponse plus rapide.

## Licence

Toutes les illustrations sont par défaut sous licence [CC By Sa 4](https://creativecommons.org/licenses/by-sa/4.0/) c'est à dire que vous êtes autorisé à utiliser, partager, modifier, redistribuer ces illustrations à condition de systématiquement citer l'auteur et de conserver cette licence pour votre travail dérivé.

## Des mots

Voici une liste non exaustive de sentiments que vous pouvez illustrer :
* [calme](/illustrations/calme/)
* peur
* frustré·e
* suprise
* sereine
* [passionné·e](/illustrations/passionne/)
* [perdu·e](/illustrations/perdu/)
* impatient·e
* dégouté·e
* épuisé·e
* triste
* fort·e
* désespéré·e
* [seul·e](/illustrations/seul/)
* enthousiaste
* inquiète
* excité·e
* destabilisé·e
* en insécurité
* en sécurité
* déçu·e
* curieux·se
* en colère
* vivant·e

Ainsi qu'une liste non exaustive de Besoins :

* sécurité
* sens
* partage
* harmonie
* échange
* coopération
* appartenance
* apprendre
* calme
* réciprocité
* simplicité
* célébration
* comprendre
* amour
* soutien
* confiance
* stimulation
* reconnaissance
* évolution
* créativité
* autonomie
* affirmation de soi
* estime de soi
* faire le deuil
* clarté
* liberté
* soin
* acceptation
* bienveillance
* contribuer
* Abri
* Alimentation, hydratation, evacuation
* Expression sexuelle
* Lumière
* Mouvement, repos
* Reproduction
* Respiration
* Préservation
* Réconfort
* Appréciation
* Chaleur humaine
* Considération
* Donner et recevoir (de l'attention, affectation, amour, tendresse)
* Empathie
* Intimité, proximité
* Respect de soi / de l'autre
* Stimulation
* Défoulement
* Récréation
* Ressourcement
* Authenticité
* Conscience
* Inspiration
* Intégrité
* Paix
* Réalisation
* Sincérité, honnêteté (qui nous permet de tirer des leçons de nos limites)
* Spiritualité
* Choisir ses rêves/objectifs/valeurs
* Choisir les moyens de réaliser ses rêves

